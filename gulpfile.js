var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps'); //this dependency allows to see the exact file where was write the css 
var autoprefixer = require('gulp-autoprefixer'); //this dependency adds the prefixes for browsers like mz, safari
var plumber = require('gulp-plumber');// This add catch to error and doesnt allow to stop the tasks execution
var browserify = require('gulp-browserify');

gulp.task('browserify', function() {
    // Single entry point to browserify 
    gulp.src('src/app.js')
        .pipe(plumber())
        .pipe(browserify({
          insertGlobals : true,
          debug : !gulp.env.production
        }))
        .pipe(gulp.dest('bundle'))
});

gulp.task('sass',function() {
    return gulp.src('src/main.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass()) // to listen a show errors in console, the task does not stop
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
         }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('bundle'))
});

gulp.task('watch', ['sass', 'browserify'], function() {
    gulp.watch('src/**/*.scss', ['sass']);
    gulp.watch('src/**/*.js', ['browserify']);
});

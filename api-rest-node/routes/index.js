'use strict';

const express = require('express');
const api = express.Router();
const UserController = require('../controllers/user')
const authMiddleware = require('../middlewares/auth');

api.post('/signup', UserController.singUp)

api.post('/signIn', UserController.singIn)

api.delete('/deleteUser/:user', authMiddleware.isAuth, UserController.deleteUser)

api.put('/updateUser/:user', authMiddleware.isAuth, UserController.updateUser)

api.put('/voteUser/:user', authMiddleware.isAuth, UserController.addUserVote);

api.get('/usersVotes', UserController.getUsersVotes);

module.exports = api;
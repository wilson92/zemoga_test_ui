'use strict';
const mongoose = require('mongoose');
const app = require('./app');
const config = require('./config');

mongoose.connect(config.db, (err, res) => {
    if (err) {
        console.log(`Error conecting to data base ${err}`);
    }
    console.log('Conection to data base stablished');

    app.listen(config.port, () => {
        console.log(`API rest running on port: ${config.port}`);
    })
})


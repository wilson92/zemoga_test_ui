'use strict';

const mongoose = require('mongoose');
const User = require('../models/user');
const service = require('../services');

function singUp (req, res) {
    const user = new User({
        user: req.body.user,
        marriageStatus: req.body.marriageStatus,
        password: req.body.password,
        age: req.body.age
    });

    user.save((err) => {
        if (err) {
            return res.status(500).send(`Errore creating user: ${err}`)
        }

        return res.status(200).send({
            user: user.user,
            token: service.createToken(user)
        })
    });
}

function singIn (req, res) {
    User.find({ user: req.body.user}, (err, user) => {
        if (err) {
            return res.status(500).send( { message: err})
        }
        if (user.length === 0) {
            return res.status(404).send( { message: 'User not found'})
        }
        req.user = user;
        res.status(200).send({
            message: 'singIn succesful',
            user: user[0].user,
            token: service.createToken(user)
        })
    })
}

function deleteUser (req, res) {
    User.find({ user: req.params.user}, (err, user) => {
        if (err) {
            return res.status(500).send( { message: err});
        }
        if (!user[0]) {
            return res.status(404).send( { message: 'User not found'});
        }       
        user[0].remove( err => {
            if (err) {
                return res.status(500).send({message: `Error performing operation: ${error}`})
            }

            res.status(200).send({message:'User was deleted'})
        })
        
    })
}

function updateUser (req, res) {
    User.findOneAndUpdate({user: req.params.user}, req.body, (err, userUpdated) => {
        if (err) {
             return res.status(500).send({message: `Error updating user: ${error}`});
        }
        res.status(200).send({
            message: 'The user was updated.'
        })
    })
}

function addUserVote (req, res) {
    let userData;
    let error;
    User.find({ user: req.params.user}, (err, user) => {
        if (err) {
            return res.status(500).send( { message: err});
        }
        if (!user[0]) {
            return res.status(404).send( { message: 'User not found'});
        }   
        console.log(req.body)
        
        req.body.type === 'positive' ? user[0].votesPositives.push(req.body.candidate) :  user[0].votesNegatives.push(req.body.candidate);

        User.findOneAndUpdate({user: req.params.user}, user[0], (err, userUpdated) => {
            if (err) {
                return res.status(500).send({message: `Error updating user: ${error}`});
            }
            res.status(200).send({
                message: 'Vote saved.'
            })
        })  
    })

    
    
}

function getUsersVotes (req, res) {
    User.find({}, (err, users) => {
        if (err) {
            return res.status(500).send({message: `Error performing operation: ${error}`})
        }

        if (!users) {
            return res.status(404).send({message: 'There are not users stored'});
        }
        let response = [];
        users.forEach((user) => {
            response.push({
                user: user.user,
                votesPositives: user.votesPositives,
                votesNegatives: user.votesNegatives
            })
        });
        res.status(200).send({ response })
    })
}

module.exports = {
    singUp,
    singIn,
    deleteUser,
    updateUser,
    addUserVote,
    getUsersVotes
}
'use strict';

var angular = require('angular');
require('angular-ui-router');

// Pages
var HomePage = require('./pages/home-page');
var LoginPage = require('./pages/login-page');

//components
var CandidateInfoComponent = require('./components/candidate-info');
var MainCandidateComponent = require('./components/main-candidate');

// Config
var app = angular.module('testApp', ['ui.router']);

app.config([
    '$stateProvider',
    '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/home");
        $stateProvider
        .state('home', {
            url: "/home",
            templateUrl: './pages/home-page/home.html',
            controllerAs: 'model',
            controller: HomePage.controller,
            resolve: HomePage.controller.resolve,
        })
        .state('past', {
            url: "/past",
            templateUrl: './pages/generic-pages/past-trials.html',            
        })
        .state('how', {
            url: "/how",
            templateUrl: './pages/generic-pages/how-it-wotks.html',            
        })
        .state('login', {
            url: "/login",
            templateUrl: './pages/login-page/login-page.html',
            controllerAs: 'model',
            controller: LoginPage.controller,          
        });
    }
]);

//components
app.component('candidateInfo', CandidateInfoComponent.config);
app.component('mainCandidate', MainCandidateComponent.config);

//services
app.service('EndPointService', function () {
    this.getEndPoint = function () {
        return 'http://localhost:3000/api/';
    }
})
app.service('HomeServices', ['$http', 'EndPointService', HomePage.services]);
app.service('LoginServices', ['$http', 'EndPointService', LoginPage.services]);
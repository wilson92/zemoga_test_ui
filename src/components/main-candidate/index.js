'use strict';
var MainCandidateController = require('./main-candidate-controller.js');

module.exports = {
    config: {
        templateUrl: './components/main-candidate/main-candidate.html',
        bindings: {
            info: '='
        },
        controllerAs: 'model',
        controller:  MainCandidateController

    }
};

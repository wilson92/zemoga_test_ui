'use strict';
var CandidateInfoController = require('./candidate-info-controller.js');

module.exports = {
    config: {
        templateUrl: './components/candidate-info/candidate-info.html',
        bindings: {
            info: '=',
            index: '<',
            saveCallback: '<',
            resetCallback: '<'
        },
        controllerAs: 'model',
        controller:  CandidateInfoController

    }
};

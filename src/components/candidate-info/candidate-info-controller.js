'use strict';

var CandidateInfoController = [
    'HomeServices',
    function(HomeServices) {
        var model = this;
        var positiveVotesBar; 
        var negativeVotesBar;

        model.selectedVote = null;
        model.voteAlreadySaved = false;
        
        var updateVotesBar = function () {
            var totalVotes = model.info.votes.positive + model.info.votes.negative;
            var positivePercentage = model.info.votes.positive * 100 / totalVotes;
            var negativePercentage = model.info.votes.negative * 100 / totalVotes;
            
            positiveVotesBar.style.width = positivePercentage + '%';
            negativeVotesBar.style.width = negativePercentage + '%';
        };

        var sendVoteToServer = function () {
            HomeServices.addUserVote({
                type: model.selectedVote,
                candidate: model.info.name
            }, function (response) {
                model.saveCallback();
                updateVotesBar();
            }, function (err) {
                model.resetCallback();
                model.voteAlreadySaved = false;
            })
        }

        var addPositiveVote = function () {  
            model.info.votes.positive++;
            sendVoteToServer();
        };

        var addNegativeVote = function () {            
            model.info.votes.negative++;
            sendVoteToServer();
        };

        model.$onInit = function () {       
            var candidateContainer = document.getElementsByClassName('candidate-container')[model.index];
            candidateContainer.style.backgroundImage = 'url(' + model.info.image + ')';
            positiveVotesBar = document.getElementsByClassName('candidate-positive-votes')[model.index];
            negativeVotesBar = document.getElementsByClassName('candidate-negative-votes')[model.index];
            updateVotesBar();
        }

        model.calcVotesPercentage = function (votesNumber) {
            var totalVotes = model.info.votes.positive + model.info.votes.negative;
            var percentage = votesNumber * 100/ totalVotes;
            return (percentage + '').indexOf('.') > -1 ? percentage.toFixed(2) :  percentage.toFixed(0);

        }

        model.setVoteOption = function (value) {
            model.selectedVote = value;
        }

        model.voteAgain = function () {
            model.voteAlreadySaved = false;
            model.selectedVote = null;
        }

        model.saveVote = function () {
            if (model.selectedVote == null) {
                return;
            }
            model.selectedVote === 'positive' ? addPositiveVote() : addNegativeVote();
            model.voteAlreadySaved = true;
        }

        model.verifyMajorVotes = function (param) {
            return param === 'positive' ? (model.info.votes.positive > model.info.votes.negative) : (model.info.votes.positive < model.info.votes.negative);
        }

    }
];

module.exports = CandidateInfoController;

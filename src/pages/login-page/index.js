'use strict';
var LoginController = require('./login-page-controller.js');
var LoginServices = require('./login-page-services.js');

module.exports = {
    controller: LoginController,
    services: LoginServices
};
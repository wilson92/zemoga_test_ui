'use strict';

var LoginController = [
    '$state','LoginServices',
    function($state, LoginServices) {
        var model = this;
        model.logUser = '';
        model.logPass = '';

        model.signUpUser = '';
        model.signUpPass = '';
        model.signUpUserMarriageStatus = '';
        model.signUpUserAge = 1;

        var persistUserInfo = function (response) {
            localStorage.setItem('user', JSON.stringify({
                user: response.data.user,
                token: response.data.token
            }))
        };

        var successCallback = function (response) {                
            persistUserInfo(response);
            $state.go('home');
        }

        var errorCallback = function (err) {
            alert(err.data.message)
        }

        model.logIn = function () {
            var dataToSend = {
                user:  model.logUser,
                pass:  model.logPass
            };
            LoginServices.logIn(dataToSend, successCallback, errorCallback);
        }

        model.signUp = function () {
            var dataToSend = {
                user: model.signUpUser,
                password: model.signUpPass,
                marriageStatus: model.signUpUserMarriageStatus,
                age: model.signUpUserAge
            };
            LoginServices.signUp(dataToSend, successCallback, errorCallback);
        }  
        		
    }
];

module.exports = LoginController;

'use strict';
var LoginServices = function($http, EndPointService) {
	var END_POINT = EndPointService.getEndPoint();
	
	this.logIn = function (body, successCallback, errorCallback) {
		return  $http({
				method: 'POST',
				url: END_POINT + 'signIn',
				data: JSON.stringify(body)
			}).then(function (response) {                
                successCallback(response)
            }, function (err) {
                errorCallback(err);
            })
		
	}

	this.signUp = function (body, successCallback, errorCallback) {
		return  $http({
				method: 'POST',
				url: END_POINT + 'signUp',
				data: JSON.stringify(body)
			}).then(function (response) {                
                successCallback(response)
            }, function (err) {
                errorCallback(err);
            })
		
	}
};

module.exports = LoginServices;
'use strict';
var HomeServices = function($http, EndPointService) {
	var END_POINT = EndPointService.getEndPoint();

	this.getInfo = function() {		
		return  $http.get('/src/info.json');
	};

	this.saveData = function (data) {
		localStorage.setItem("candidates", JSON.stringify(data));
	}	

	this.addUserVote = function (body, successCallback, errorCallback) {
		var userDataLocal = JSON.parse(localStorage.user);
		var user = userDataLocal.user;
		var token = userDataLocal.token;

		return  $http({
				method: 'PUT',
				url: END_POINT + 'voteUser/' + user,
				headers: {
					'Authorization': 'Bearer ' + token,
				},
				data: JSON.stringify(body)
			}).then(function (response) {                
                successCallback(response)
            }, function (err) {
                errorCallback(err);
            })
		
	}
};

module.exports = HomeServices;
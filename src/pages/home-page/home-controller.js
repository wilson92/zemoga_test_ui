'use strict';

var HomeController = [
	'$state', 'HomeServices', 'datafromService',
	function($state, HomeServices, datafromService) {
        var model = this;
		var mainSection = document.getElementById('main-section');
		model.candidatesList = [];
		model.mainCandidate = {}; 
		model.showSiteDescription = true; 
		model.logInSingUpText = '';

		var initData = function () {
			model.candidatesList = datafromService.candidates;
			model.mainCandidate = datafromService.mainCandidate; 
			model.showSiteDescription = true; 
			model.logInSingUpText = localStorage.user ? 'Log Out' : 'Log In /Sign Up'; 
		};
        initData();
		

		mainSection.style.backgroundImage = 'url(' + model.mainCandidate.image + ')';
		
		model.goToOtherPage = function (page) {
			$state.go(page)
		}

		model.closeSiteDescription = function () {
			model.showSiteDescription = false; 
		}

		model.saveDataCallback = function () {
			HomeServices.saveData({
				mainCandidate: model.mainCandidate,
				candidates: model.candidatesList
			});
		}

		model.resetDataCallback = function () {
			datafromService =  localStorage.candidates ? JSON.parse(localStorage.candidates) : HomeServices.getInfo()
				.then( function( response ) {
					return response.data;
				});
			initData();
		}	
	}
];

HomeController.resolve = {
	datafromService: [
		'HomeServices',
		function(HomeServices) {
			return localStorage.candidates ? JSON.parse(localStorage.candidates) : HomeServices.getInfo()
				.then( function( response ) {
					return response.data;
				});
		}
	]
};
module.exports = HomeController;

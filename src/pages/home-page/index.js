'use strict';
var HomeController = require('./home-controller.js');
var HomeServices = require('./home-services.js');

module.exports = {
    controller: HomeController,
    services:HomeServices
};